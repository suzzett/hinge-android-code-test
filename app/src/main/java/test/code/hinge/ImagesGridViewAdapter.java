package test.code.hinge;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

/**
 * Created by suzzett on 7/2/17.
 *
 */

public class ImagesGridViewAdapter extends ArrayAdapter {
	private static final String TAG = ImagesGridViewAdapter.class.getSimpleName();
	private Context context;
	private int layout_resource_id;
	public ImagesGridViewAdapter(Context context, int resource) {
		super(context, resource);
		this.context = context;
		this.layout_resource_id = resource;
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, @NonNull ViewGroup parent) {
		Log.d(TAG, "get view called for "+position);
		View row = convertView;
		ViewHolder holder;
		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layout_resource_id, parent, false);
			holder = new ViewHolder();
			holder.image = (ImageView) row.findViewById(R.id.cell_images_grid_image);
			row.setTag(holder);
		} else {
			holder = (ViewHolder) row.getTag();
		}

		ImagesList.ImageObject item = ImagesList.getInstance().getImageAt(position);
		holder.image.setImageBitmap(item.getImage());
		return row;
	}

	private static class ViewHolder {
		ImageView image;
	}
}

package test.code.hinge.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import test.code.hinge.R;
import test.code.hinge.fragments.HomePageDefaultFragment;

public class HomePage extends AppCompatActivity implements HomePageDefaultFragment.OnFragmentInteractionListener {

	private final String TAG = HomePage.class.getSimpleName();
	private int fragment_container;
	private Fragment current_fragment;
	private final String CURRENT_FRAGMENT_TAG = "current_fragment";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_page);
		fragment_container = R.id.home_page_fragment_container;

		current_fragment = HomePageDefaultFragment.newInstance();
		if (savedInstanceState != null) {
			current_fragment = getFragmentManager().getFragment(savedInstanceState, CURRENT_FRAGMENT_TAG);
		}
		changeFragment();
		setActionBarTitle("Home");
	}

	private void changeFragment() {
		//TODO Add animation on fragment change
		// Create new fragment and transaction
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		// Replace whatever is in the fragment_container view with this fragment,
		// and add the transaction to the back stack if needed
		if (current_fragment != null && transaction != null) {
			transaction.replace(fragment_container, current_fragment);
			transaction.addToBackStack(null);
			// Commit the transaction
			transaction.commit();
		}
	}

	@Override
	public void onFragmentInteraction(Uri uri) {

	}

	public void setActionBarTitle(String title) {
		try {
			getSupportActionBar().setTitle(title);
		} catch (NullPointerException npe) {
			npe.printStackTrace();
		}
	}
}

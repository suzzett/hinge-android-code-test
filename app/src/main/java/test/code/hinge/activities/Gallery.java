package test.code.hinge.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import test.code.hinge.Globals;
import test.code.hinge.R;
import test.code.hinge.fragments.GalleryDefaultFragment;
import test.code.hinge.fragments.HomePageDefaultFragment;

public class Gallery extends AppCompatActivity {
	private final String TAG = Gallery.class.getSimpleName();
	private int fragment_container;
	private Fragment current_fragment;
	private final String CURRENT_FRAGMENT_TAG = "current_fragment";
	private int current_image = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gallery);
		fragment_container = R.id.gallery_fragment_container;
		Intent intent = getIntent();
		if (intent != null) {
			current_image = intent.getIntExtra(Globals.GALLERY_IMAGE_POSITION, 0);
		}
		current_fragment = GalleryDefaultFragment.newInstance(current_image);
		if (savedInstanceState != null) {
			current_fragment = getFragmentManager().getFragment(savedInstanceState, CURRENT_FRAGMENT_TAG);
		}
		changeFragment();
	}

	private void changeFragment() {
		//TODO Add animation on fragment change
		// Create new fragment and transaction
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		// Replace whatever is in the fragment_container view with this fragment,
		// and add the transaction to the back stack if needed
		if (current_fragment != null && transaction != null) {
			transaction.replace(fragment_container, current_fragment);
			transaction.addToBackStack(null);
			// Commit the transaction
			transaction.commit();
		}
	}

	@Override
	public void onBackPressed() {
		this.finish();
	}

	public void setActionBarTitle(String title) {
		try {
			getSupportActionBar().setTitle(title);
		} catch (NullPointerException npe) {
			npe.printStackTrace();
		}
	}
}
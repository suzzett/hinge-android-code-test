package test.code.hinge;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

/**
 * Created by suzzett on 7/1/17.
 *
 */

public class ImagesList {
	private ArrayList<ImageObject> images_list;
	public static ImagesList all_image_list;

	private ImagesList() {
		images_list = new ArrayList<>();
	}

	public static ImagesList getInstance() {
		if (all_image_list == null) {
			all_image_list = new ImagesList();
		}
		return all_image_list;
	}

	public void addImage(String image_url, String image_name, String image_description) {
		images_list.add(new ImageObject(image_url, image_name, image_description, images_list.size()));
	}

	public void addImage(String image_url, String image_name, String image_description, Bitmap bitmap) {
		images_list.add(new ImageObject(image_url, image_name, image_description, bitmap, images_list.size()));
	}

	public ImageObject getImageAt(int index) {
		if (index < images_list.size()) {
			return images_list.get(index);
		}
		return null;
	}

	public ArrayList<ImageObject> getImagesList() {
		return images_list;
	}

	public ArrayList<String> getImageUrls() {
		ArrayList<String> image_urls = new ArrayList<>();
		for (ImageObject io : images_list) {
			image_urls.add(io.getImageUrl());
		}
		return image_urls;
	}

	public int size() {
		return images_list.size();
	}

	public String getImageUrlAt(int position) {
		return images_list.get(position).getImageUrl();
	}

	public String getImageTitleAt(int position) {
		return images_list.get(position).getImageName();
	}

	public void deleteImageAt(int position) {
		images_list.remove(position);
	}

	public String getImageDescriptionAt(int position) {
		return images_list.get(position).getImageDescription();
	}

	public Bitmap getImageBitmapAt(int position) {
		return images_list.get(position).getImage();
	}

	public void clear() {
		images_list.clear();
	}

	public class ImageObject {
		private String image_url;
		private String image_name;
		private String image_description;
		private Bitmap image;
		private int image_index;
		private ImageObject(String image_url, String image_name, String image_description, int image_index) {
			setImageName(image_name);
			setImageUrl(image_url);
			setImageDescription(image_description);
			setImageIndex(image_index);
			Glide
				.with(App.get())
				.load(image_url)
				.asBitmap()
				.into(new SimpleTarget<Bitmap>(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL) {
					@Override
					public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
						setImageBitmap(resource);
						SaveForOffline save_for_offline_task = new SaveForOffline();
						save_for_offline_task.execute();
					}

					@Override
					public void onLoadFailed(Exception e, Drawable errorDrawable) {
						Bitmap error_drawable = BitmapFactory.decodeResource(App.get().getResources(), R.drawable.failure);
						setImageBitmap(error_drawable);
						SaveForOffline save_for_offline_task = new SaveForOffline();
						save_for_offline_task.execute();
					}
				});
		}

		public ImageObject(String image_url, String image_name, String image_description, Bitmap bitmap, int image_index) {
			setImageName(image_name);
			setImageUrl(image_url);
			setImageDescription(image_description);
			setImageIndex(image_index);
			setImageBitmap(bitmap);
		}

		private String getImageName() {
			return this.image_name;
		}

		private String getImageUrl() {
			return this.image_url;
		}

		private String getImageDescription() {
			return this.image_description;
		}

		Bitmap getImage() {
			return this.image;
		}

		private void setImageName(String image_name) {
			this.image_name = image_name;
		}

		private void setImageUrl(String image_url) {
			this.image_url = image_url;
		}

		private void setImageDescription(String image_desription) {
			this.image_description = image_desription;
		}

		private void setImageIndex(int image_index) {
			this.image_index = image_index;
		}

		public void setImageBitmap(Bitmap image_bitmap) {
			this.image = image_bitmap;
		}

		private class SaveForOffline extends AsyncTask<Void, Void, String> {
			private final String TAG = ImagesList.class.getSimpleName()+"."+SaveForOffline.class.getSimpleName();
			public SaveForOffline() {}

			protected void onPreExecute() {}
			@Override
			protected String doInBackground(Void... params) {
				Utils.saveImageForOffline(ImageObject.this.image, ImageObject.this.image_index, ImageObject.this.image_name, ImageObject.this.image_description, ImageObject.this.image_url, App.get());
				return null;
			}
			protected void onPostExecute(String result) {}
		}
	}
}

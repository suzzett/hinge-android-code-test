package test.code.hinge;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import test.code.hinge.activities.Gallery;

/**
 * Created by suzzett on 7/2/17.
 *
 */

public class ImageThumbnailAdapter  extends RecyclerView.Adapter<ImageThumbnailAdapter.ImagesViewHolder> {
	private final static String TAG = ImageThumbnailAdapter.class.getSimpleName();
	private Context context;
	private int screen_width = 160;
	public ImageThumbnailAdapter(Context context, int screen_width) {
		this.context = context;
		this.screen_width = screen_width;
	}

	@Override
	public ImageThumbnailAdapter.ImagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View image_view_row = inflater.inflate(R.layout.cell_images_grid, parent, false);
		ImagesViewHolder view_holder = new ImagesViewHolder(image_view_row);
		return view_holder;
	}

	@Override
	public void onBindViewHolder(ImageThumbnailAdapter.ImagesViewHolder holder, int position) {
		final ImageView image_view = holder.image_view;
		Glide.with(context)
						.load(ImagesList.getInstance().getImageUrlAt(position))
						.placeholder(R.drawable.waiting_animation)
						.override(screen_width, screen_width)
						.error(R.drawable.failure2)
						.centerCrop()
						.into(image_view);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemViewType(int position) {
		return position;
	}

	@Override
	public int getItemCount() {
		return ImagesList.getInstance().size();
	}

	public class ImagesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		private final String TAG = ImageThumbnailAdapter.TAG+"."+ImagesViewHolder.class.getSimpleName();
		public ImageView image_view;
		public ImagesViewHolder(View itemView) {
			super(itemView);
			image_view = (ImageView) itemView.findViewById(R.id.cell_images_grid_image);
			image_view.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			Log.d(TAG, "Clicked at "+getAdapterPosition());
			Intent gallery_activity = new Intent(context, Gallery.class);
			int position = getAdapterPosition();
			gallery_activity.putExtra(Globals.GALLERY_IMAGE_POSITION, position);
			context.startActivity(gallery_activity);
		}
	}
}

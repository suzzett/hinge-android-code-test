package test.code.hinge;

import android.app.Application;
import android.content.Context;
import android.util.Log;

/**
 * Created by suzzett on 7/5/17.
 * Getting context in singletons to avoid memory leak
 */

public class App extends Application {
	private static final String TAG = App.class.getSimpleName();
	private static App instance;
	public static App get() {
		return instance;
	}
	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "Application created.");
		instance = this;
	}
}

package test.code.hinge;

/**
 * Created by suzzett on 7/1/17.
 */

public class Globals {
	public static final String GALLERY_IMAGE_POSITION = "gallery_image_position";
	public static final String OFFLINE_DIR_TAG = "offline_images";
	public static final String IMAGES_LIST_URL = "https://hinge-homework.s3.amazonaws.com/client/services/homework.json";
	public static final String OFFLINE_FILE_FORMAT = ".jpg";
	public static final String IMAGES_LIST_PREFS = "images_list_shared_pref_key";
}

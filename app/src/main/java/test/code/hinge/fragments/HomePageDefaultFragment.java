package test.code.hinge.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import test.code.hinge.App;
import test.code.hinge.Globals;
import test.code.hinge.ImageThumbnailAdapter;
import test.code.hinge.ImagesList;
import test.code.hinge.R;
import test.code.hinge.Utils;
import test.code.hinge.activities.HomePage;

public class HomePageDefaultFragment extends Fragment {
	private final String TAG = HomePageDefaultFragment.class.getSimpleName();
	private OnFragmentInteractionListener mListener;
	private FrameLayout progress_bar;
	private Context context;
	private ImageThumbnailAdapter adapter;
	private RecyclerView recyclerView;
	RecyclerView.LayoutManager layoutManager;

	public HomePageDefaultFragment() {
		// Required empty public constructor
	}

	public static HomePageDefaultFragment newInstance() {
		HomePageDefaultFragment fragment = new HomePageDefaultFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		if (getArguments() != null) {
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.fragment_default_home_page, menu);
		Drawable delete_drawable = menu.getItem(0).getIcon();
		delete_drawable.mutate();
		delete_drawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// handle item selection
		switch (item.getItemId()) {
			case R.id.default_home_page_get_images_list:
				Toast.makeText(getActivity(), "Refreshing", Toast.LENGTH_SHORT).show();
				getImages();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View v = inflater.inflate(R.layout.fragment_home_page_default, container, false);
		progress_bar = (FrameLayout) v.findViewById(R.id.progressBarHolder);
		recyclerView = (RecyclerView) v.findViewById(R.id.home_page_default_rv);
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int screenWidth = displaymetrics.widthPixels;
		layoutManager = new GridLayoutManager(context, 2);
		recyclerView.setHasFixedSize(true);
		recyclerView.setLayoutManager(layoutManager);
		adapter = new ImageThumbnailAdapter(context, screenWidth);
		recyclerView.setAdapter(adapter);
		if (ImagesList.getInstance().size() < 1) {
			getImages();
		}
	}
	// TODO: Rename method, update argument and hook method into UI event
	public void onButtonPressed(Uri uri) {
		if (mListener != null) {
			mListener.onFragmentInteraction(uri);
		}
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		this.context = context;
		if (context instanceof OnFragmentInteractionListener) {
			mListener = (OnFragmentInteractionListener) context;
		} else {
			throw new RuntimeException(context.toString()
							                           + " must implement OnFragmentInteractionListener");
		}
	}

	// For devices on API < 23
	@Override
	public void onAttach(Activity context) {
		super.onAttach(context);
		this.context = context;
		if (context instanceof OnFragmentInteractionListener) {
			mListener = (OnFragmentInteractionListener) context;
		}
		else {
			throw new RuntimeException(context.toString()
							                           + " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	public interface OnFragmentInteractionListener {
		// TODO: Update argument type and name
		void onFragmentInteraction(Uri uri);
	}

	private void getImages() {
		if (Utils.isNetworkAvailable(context)) {
			GetImagesListFromNetwork image_list_async_task = new GetImagesListFromNetwork();
			image_list_async_task.execute();
		} else {
			GetImagesListLocally image_list_async_task = new GetImagesListLocally();
			image_list_async_task.execute();
		}
	}
	private class GetImagesListLocally extends AsyncTask<Void, Void, Void> {
		private final String TAG = HomePageDefaultFragment.class.getSimpleName()+"."+GetImagesListLocally.class.getSimpleName();

		protected void onPreExecute() {
			progress_bar.setVisibility(View.VISIBLE);
			int size = ImagesList.getInstance().size();
			ImagesList.getInstance().clear();
			adapter.notifyItemRangeRemoved(0, size);
		}

		@Override
		protected Void doInBackground(Void... params) {
			Utils.loadImageFromStorage(App.get());
			return null;
		}

		protected void onPostExecute(Void result) {
			//parse JSON data
			recyclerView.setAdapter(adapter);
			recyclerView.setLayoutManager(layoutManager);
			adapter.notifyItemRangeInserted(0, ImagesList.getInstance().size());
			progress_bar.setVisibility(View.GONE);
		}
	}


	private class GetImagesListFromNetwork extends AsyncTask<Void, Void, String> {
		private final String TAG = HomePageDefaultFragment.class.getSimpleName()+"."+GetImagesListFromNetwork.class.getSimpleName();

		protected void onPreExecute() {
			progress_bar.setVisibility(View.VISIBLE);
			int size = ImagesList.getInstance().size();
			ImagesList.getInstance().clear();
			adapter.notifyItemRangeRemoved(0, size);
		}

		@Override
		protected String doInBackground(Void... params) {
			HttpURLConnection connection = null;
			BufferedReader reader = null;

			try {
				URL url = new URL(Globals.IMAGES_LIST_URL);
				connection = (HttpURLConnection) url.openConnection();
				connection.connect();
				InputStream stream = connection.getInputStream();
				reader = new BufferedReader(new InputStreamReader(stream));
				StringBuilder buffer = new StringBuilder();
				String line = "";
				while ((line = reader.readLine()) != null) {
					buffer.append(line).append("\n");
					Log.d("Response: ", "> " + line);
				}
				return buffer.toString();
			}
			catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (connection != null) {
					connection.disconnect();
				}
				try {
					if (reader != null) {
						reader.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return "";
		}

		protected void onPostExecute(String result) {
			//parse JSON data
			try {
				JSONArray jArray = new JSONArray(result);
				for(int i=0; i < jArray.length(); i++) {
					JSONObject jObject = jArray.getJSONObject(i);
					Log.d(TAG, " i = "+i+", object = "+jObject.toString());
					String image_url = jObject.has("imageURL")?jObject.get("imageURL").toString():"";
					String image_description = jObject.has("imageDescription")?jObject.get("imageDescription").toString():"";
					String image_image_name = jObject.has("imageName")?jObject.get("imageName").toString():"";
					ImagesList.getInstance().addImage(image_url, image_image_name, image_description);
				}
				recyclerView.setAdapter(adapter);
				recyclerView.setLayoutManager(layoutManager);
				adapter.notifyItemRangeInserted(0, ImagesList.getInstance().size());
			} catch (JSONException e) {
				Log.e(TAG, "Error: " + e.toString());
			} finally { // catch (JSONException e)
				progress_bar.setVisibility(View.GONE);
			}
		}
	}
}

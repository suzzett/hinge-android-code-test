package test.code.hinge.fragments;

import android.app.Fragment;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import test.code.hinge.App;
import test.code.hinge.ImagesList;
import test.code.hinge.R;
import test.code.hinge.Utils;
import test.code.hinge.activities.Gallery;

public class GalleryDefaultFragment extends Fragment {
	private static final String TAG = GalleryDefaultFragment.class.getSimpleName();
	private static final String CURRENT_IMAGE_POSITION_TAG = "current_image_position";
	private int current_image_position = 0;
	private ImageView image_view;
	private TextView image_description_tv;
	private Handler handler;
	private boolean keep_images_changing = true;

	public GalleryDefaultFragment() {
		// Required empty public constructor
	}

	public static GalleryDefaultFragment newInstance(int current_image_position) {
		GalleryDefaultFragment fragment = new GalleryDefaultFragment();
		Bundle args = new Bundle();
		args.putInt(CURRENT_IMAGE_POSITION_TAG, current_image_position);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		if (getArguments() != null) {
			current_image_position = getArguments().getInt(CURRENT_IMAGE_POSITION_TAG);
		}
		handler=new Handler();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.fragment_default_gallery, menu);
		Drawable delete_drawable = menu.getItem(0).getIcon();
		delete_drawable.mutate();
		delete_drawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// handle item selection
		switch (item.getItemId()) {
			case R.id.default_gallery_menu_delete:
				Log.d(TAG, "Delete pressed");
				ImagesList.getInstance().deleteImageAt(current_image_position);
				Toast.makeText(getActivity(), "Deleted", Toast.LENGTH_SHORT).show();
				changeItemPosition(-1);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private synchronized void  changeItemPosition(int change) {
		current_image_position += change;
		if (current_image_position >= ImagesList.getInstance().size()) current_image_position = 0;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View v = inflater.inflate(R.layout.fragment_gallery_default, container, false);
		image_view = (ImageView) v.findViewById(R.id.gallery_default_image);
		image_description_tv = (TextView) v.findViewById(R.id.gallery_default_image_description_tv);
		changeImage(current_image_position);
		return v;
	}

	public Runnable timedImageChanges = new Runnable()
	{
		@Override
		public void run()
		{
			if(!keep_images_changing) {
				handler.removeCallbacks(this);
				return;
			}
			changeItemPosition(1);
			changeImage(current_image_position);
			handler.postDelayed(timedImageChanges, 2000);
		}
	};

	@Override
	public void onPause() {
		super.onPause();
		keep_images_changing = false;
	}

	@Override
	public void onResume() {
		super.onResume();
		keep_images_changing = true;
		handler.postDelayed(timedImageChanges, 2000);
	}
	@Override
	public void onDetach() {
		super.onDetach();
	}

	private void changeImage(int to_position) {
		image_description_tv.setText(ImagesList.getInstance().getImageDescriptionAt(to_position));
		if (Utils.isNetworkAvailable(App.get())) {
			Glide.with(getActivity())
							.load(ImagesList.getInstance().getImageUrlAt(to_position))
							.placeholder(R.drawable.waiting_animation)
							.error(R.drawable.failure2)
							.into(image_view);
		} else {
			image_view.setImageBitmap(ImagesList.getInstance().getImageBitmapAt(to_position));
		}
		((Gallery) getActivity()).setActionBarTitle((current_image_position+1)+"/"+ImagesList.getInstance().size()+" - "+ImagesList.getInstance().getImageTitleAt(to_position));
	}
}

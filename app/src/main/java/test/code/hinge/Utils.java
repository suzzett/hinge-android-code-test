package test.code.hinge;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by suzzett on 7/5/17.
 *
 * All helper methods
 */

public class Utils {
	private static final String TAG = Utils.class.getSimpleName();
	public static void saveImageForOffline(Bitmap bitmapImage, int image_index, String image_title, String image_description, String image_url, Context context){
		File offline_images_dir = context.getDir(Globals.OFFLINE_DIR_TAG, Context.MODE_PRIVATE);
		String image_name = Integer.toString(image_index) + Globals.OFFLINE_FILE_FORMAT;
		File image_path = new File(offline_images_dir, Integer.toString(image_index) + Globals.OFFLINE_FILE_FORMAT);
		FileOutputStream fos = null;
		try {
			Log.d(TAG, "Saving: "+image_path.getAbsolutePath());
			fos = new FileOutputStream(image_path);
			bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SharedPreferences prefs = context.getSharedPreferences(Globals.IMAGES_LIST_PREFS, Context.MODE_PRIVATE);
			prefs.edit().putString(Integer.toString(image_index)+"_title",        image_title).apply();
			prefs.edit().putString(Integer.toString(image_index)+"_description",  image_description).apply();
			prefs.edit().putString(Integer.toString(image_index)+"_file_name",    image_name).apply();
			prefs.edit().putString(Integer.toString(image_index)+"_url",          image_url).apply();
			try {
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void loadImageFromStorage(Context context)
	{
		SharedPreferences prefs = context.getSharedPreferences(Globals.IMAGES_LIST_PREFS, Context.MODE_PRIVATE);
		File offline_images_dir = context.getDir(Globals.OFFLINE_DIR_TAG, Context.MODE_PRIVATE);
		Map<String, ?> all_images_info = prefs.getAll();
		int total_images = all_images_info.size()/4;
		Log.d(TAG, "Total images: "+total_images);
		for (int image_index = 0; image_index < total_images; image_index++) {
			String image_url =            prefs.getString(Integer.toString(image_index)+"_url", "");
			String image_title =          prefs.getString(Integer.toString(image_index)+"_title", "");
			String image_description =    prefs.getString(Integer.toString(image_index)+"_description", "");
			String image_file_name =      prefs.getString(Integer.toString(image_index)+"_file_name", "");
			File image_path = new File(offline_images_dir, image_file_name);
			Log.d(TAG, "Image path: "+image_path.getAbsolutePath());
			try {
				Bitmap image = BitmapFactory.decodeStream(new FileInputStream(image_path));
        ImagesList.getInstance().addImage(image_url, image_title, image_description, image);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

}
